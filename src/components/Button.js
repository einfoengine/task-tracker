const Button = ({btntext, onClick}) => {
    return <button className="btn btn-primary" onClick={onClick} >{btntext}</button> 
}

Button.defaultProps = {
    btntext: 'Add'
}

export default Button
