import Button from './Button'

const Header = ({title, btntext}) =>{
    
    const onClick = () => {
        console.log('clicked');
    }

    return(
        <header className='d-flex justify-content-between'>
            <h1>{title}</h1>
            <Button btntext={btntext} onClick={onClick}/>
        </header>
    );
}

Header.defaultProps = {
    title: 'Task Tracker',
};

export default Header;
