import Header from './components/header';

function App() {
  return (
    <div className="App">
      <section className="header">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="card app-wrap mt-3">
                <div className="card-body">
                  <Header title='Task tracker application' btntext='add task'/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default App;
